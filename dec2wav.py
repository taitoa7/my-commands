# -*- coding: utf-8 -*-
# coding: UTF-8
import wave, array
import struct
import numpy as np
import copy as cp


#################################################################
# float 形式 の ascii file  を wav file へ変換
#################################################################

########################  USER SETTING  #########################
#infile1 = "t:/cygwin/home/ito/matlab/wave/NTT/wildflower_7sec_16bit_32000.dat"
infile1 = "t:/cygwin/home/ito/matlab/wave/NTT/array_female_6sec_16bit_fs24k_adpcmed.dat"

outfile1 = "t:/cygwin/home/ito/matlab/wave/NTT/array_female_6sec_16bit_fs24k_adpcmed.wav"
ChNum=1     #ch number  mono=1,  stereo=2
Fs=24000    #Fs
ByteWidthPerSample =  2#
#################################################################

infh1 = open(infile1)
line = infh1.readline()
yf=[]

while line:
    line = infh1.readline() # 1行ずつ read
    line = line.strip()     #chop
    if line=="":
        break
    linef=float(line)
    yf.append(linef)
    #yf.append(linef*2**(8*ByteWidthPerSample -1))

infh1.close

#サイン波を-32768から32767の整数値に変換(signed 16bit pcmへ)
#yf = [int(x * 32768.0) for x in yf]
yf = [int(x * 2**(8*ByteWidthPerSample-1)) for x in yf]

#exchange to  binary
binwave = struct.pack("h" * len(yf), *yf)

w = wave.Wave_write(outfile1)
params = (
    ChNum,                          #ch number
    ByteWidthPerSample,             #Byte width / sample
    Fs,                             #sampling rate
    len(binwave),                   #number of frames
    'NONE', 'not compressed'    # no compression
)
w.setparams(params)
w.writeframes(binwave)
w.close()
